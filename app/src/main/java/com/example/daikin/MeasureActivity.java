package com.example.daikin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import com.google.ar.core.Anchor;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Camera;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.Sun;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class MeasureActivity extends AppCompatActivity implements Node.OnTapListener, Scene.OnUpdateListener {
    private static final String TAG = MeasureActivity.class.getSimpleName();
    private static final double MIN_OPENGL_VERSION = 3.0;

    ArrayList<Float> arrayList1 = new ArrayList<>();
    ArrayList<Float> arrayList2 = new ArrayList<>();
    private ArFragment arFragment;
    private AnchorNode lastAnchorNode;
    ModelRenderable modelRenderable;
    Vector3 point1, point2;
    private MaterialButton btnClear;
    private MaterialButton btnAdd;
    private MaterialButton btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!checkIsSupportedDeviceOrFinish(this)) {
            return;
        }
        setContentView(R.layout.activity_measure);
        initView();
        initModel();
        setEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupPlane();
    }

    private void setupPlane() {
        Session session = arFragment.getArSceneView().getSession();
        if (session == null) {
            return;
        }
        Config config = session.getConfig();
//        config.setFocusMode(Config.FocusMode.AUTO);
//        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);
        config.setPlaneFindingMode(Config.PlaneFindingMode.HORIZONTAL_AND_VERTICAL);
        arFragment.getArSceneView().setupSession(session);
    }

    private void initView() {
        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        btnClear = findViewById(R.id.btn_clear);
        btnAdd = findViewById(R.id.btn_add);
        btnNext = findViewById(R.id.btn_next);
    }

    private void setEvents() {
        arFragment.setOnTapArPlaneListener(this::onTapArPlane);
        btnClear.setOnClickListener(v -> onClear());
        btnAdd.setOnClickListener(v -> onClear());
    }

    private void initModel() {
        MaterialFactory.makeTransparentWithColor(this, new Color(0F, 0F, 244F))
                .thenAccept(
                        material -> {
                            Vector3 vector3 = new Vector3(0.01f, 0.01f, 0.01f);
                            modelRenderable = ShapeFactory.makeCube(vector3, Vector3.zero(), material);
                            modelRenderable.setShadowCaster(false);
                            modelRenderable.setShadowReceiver(false);
                        });
    }

    private void onTapArPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {
        if (modelRenderable == null) {
            return;
        }

        if (lastAnchorNode == null) {
            Anchor anchor = hitResult.createAnchor();
            AnchorNode anchorNode = new AnchorNode(anchor);
            anchorNode.setParent(arFragment.getArSceneView().getScene());

            Pose pose = anchor.getPose();
            if (arrayList1.isEmpty()) {
                arrayList1.add(pose.tx());
                arrayList1.add(pose.ty());
                arrayList1.add(pose.tz());
            }
            TransformableNode transformableNode = new TransformableNode(arFragment.getTransformationSystem());
            transformableNode.setParent(anchorNode);
            transformableNode.setRenderable(modelRenderable);
            transformableNode.select();
            lastAnchorNode = anchorNode;
        } else {
            int val = motionEvent.getActionMasked();
            float axisVal = motionEvent.getAxisValue(MotionEvent.AXIS_X, motionEvent.getPointerId(motionEvent.getPointerCount() - 1));
            Log.e("Values:", String.valueOf(val) + String.valueOf(axisVal));
            Anchor anchor = hitResult.createAnchor();
            AnchorNode anchorNode = new AnchorNode(anchor);
            anchorNode.setParent(arFragment.getArSceneView().getScene());

            Pose pose = anchor.getPose();

            float distance = 0;
            if (arrayList2.isEmpty()) {
                arrayList2.add(pose.tx());
                arrayList2.add(pose.ty());
                arrayList2.add(pose.tz());
                distance = getDistanceMeters(arrayList1, arrayList2);
//                txtDistance.setText("Distance: " + String.valueOf(d));
            } else {
                arrayList1.clear();
                arrayList1.addAll(arrayList2);
                arrayList2.clear();
                arrayList2.add(pose.tx());
                arrayList2.add(pose.ty());
                arrayList2.add(pose.tz());
                distance= getDistanceMeters(arrayList1, arrayList2);
//                txtDistance.setText("Distance: " + String.valueOf(d));
            }

            TransformableNode transformableNode = new TransformableNode(arFragment.getTransformationSystem());
            transformableNode.setParent(anchorNode);
            transformableNode.setRenderable(modelRenderable);
            transformableNode.select();

            Vector3 point1, point2;
            point1 = lastAnchorNode.getWorldPosition();
            point2 = anchorNode.getWorldPosition();

            final Vector3 difference = Vector3.subtract(point1, point2);
            final Vector3 directionFromTopToBottom = difference.normalized();
            final Quaternion rotationFromAToB =
                    Quaternion.lookRotation(directionFromTopToBottom, Vector3.up());
            float finalDistance = distance;
            MaterialFactory.makeOpaqueWithColor(getApplicationContext(), new Color(0, 255, 244))
                    .thenAccept(
                            material -> {
                                ModelRenderable model = ShapeFactory.makeCube(
                                        new Vector3(.01f, .01f, difference.length()),
                                        Vector3.zero(), material);
                                model.setShadowCaster(false);
                                model.setShadowReceiver(false);
                                Node node = new Node();
                                node.setParent(anchorNode);
                                node.setRenderable(model);
                                node.setWorldPosition(Vector3.add(point1, point2).scaled(.5f));
                                node.setWorldRotation(rotationFromAToB);
                                addText(node, finalDistance, new Vector3(0f, 0.02f, 0f), anchorNode);
                            }
                    );
            lastAnchorNode = anchorNode;
        }
    }

    private void addText(Node node, float distance, Vector3 point, AnchorNode anchorNode) {
        Node infoCard = new Node();
        infoCard.setParent(node);
        infoCard.setLocalPosition(point);
//        infoCard.setLookDirection(Vector3.up(), anchorNode.getUp());

        ViewRenderable.builder()
                .setView(this, R.layout.distance_text_layout)
                .build()
                .thenAccept(
                        (renderable) -> {
                            renderable.setShadowCaster(false);
                            renderable.setShadowReceiver(false);
                            infoCard.setRenderable(renderable);
                            TextView txtDistance = renderable.getView().findViewById(R.id.txt_distance);

                            float distanceCM = changeUnit(distance, "cm");
                            txtDistance.setText(String.format(Locale.getDefault(), "%.0f cm", distanceCM));
                        })
                .exceptionally(
                        throwable -> {
                            Toast toast = Toast.makeText(this, "Unable to load andy renderable", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return null;
                        });
    }

    private void onClear() {
        List<Node> children = new ArrayList<>(arFragment.getArSceneView().getScene().getChildren());
        for (Node node : children) {
            if (node instanceof AnchorNode) {
                if (((AnchorNode) node).getAnchor() != null) {
                    ((AnchorNode) node).getAnchor().detach();
                }
            }
            if (!(node instanceof Camera) && !(node instanceof Sun)) {
                node.setParent(null);
            }
        }
        arrayList1.clear();
        arrayList2.clear();
        lastAnchorNode = null;
        point1 = null;
        point2 = null;
//        txtDistance.setText("");
    }

    private float getDistanceMeters(ArrayList<Float> arayList1, ArrayList<Float> arrayList2) {

        float distanceX = arayList1.get(0) - arrayList2.get(0);
        float distanceY = arayList1.get(1) - arrayList2.get(1);
        float distanceZ = arayList1.get(2) - arrayList2.get(2);
        return (float) Math.sqrt(distanceX * distanceX +
                distanceY * distanceY +
                distanceZ * distanceZ);
    }

    private float changeUnit(float distanceMeter, String unit) {
        if ("cm".equals(unit)) {
            return distanceMeter * 100;
        } else if ("mm".equals(unit)) {
            return distanceMeter * 1000;
        }
        return distanceMeter;
    }

    @SuppressLint("ObsoleteSdkInt")
    public static boolean checkIsSupportedDeviceOrFinish(final Activity activity) {
        if (Build.VERSION.SDK_INT < VERSION_CODES.N) {
            Log.e(TAG, "Sceneform requires Android N or later");
            Toast.makeText(activity, "Sceneform requires Android N or later", Toast.LENGTH_LONG).show();
            activity.finish();
            return false;
        }
        String openGlVersionString =
                ((ActivityManager) Objects.requireNonNull(activity.getSystemService(Context.ACTIVITY_SERVICE)))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later");
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                    .show();
            activity.finish();
            return false;
        }
        return true;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onTap(HitTestResult hitTestResult, MotionEvent motionEvent) {
//        Node node = hitTestResult.getNode();
//        Box box = (Box) node.getRenderable().getCollisionShape();
//        assert box != null;
//        Vector3 renderableSize = box.getSize();
//        Vector3 transformableNodeScale = node.getWorldScale();
//        Vector3 finalSize =
//                new Vector3(
//                        renderableSize.x * transformableNodeScale.x,
//                        renderableSize.y * transformableNodeScale.y,
//                        renderableSize.z * transformableNodeScale.z);
//        txtDistance.setText("Height: " + String.valueOf(finalSize.y));
//        Log.e("FinalSize: ", String.valueOf(finalSize.x + " " + finalSize.y + " " + finalSize.z));
        //Toast.makeText(this, "Final Size is " + String.valueOf(finalSize.x + " " + finalSize.y + " " + finalSize.z), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdate(FrameTime frameTime) {
        Frame frame = arFragment.getArSceneView().getArFrame();
//        Collection<Anchor> updatedAnchors = frame.getUpdatedAnchors();
//        for (Anchor anchor : updatedAnchors) {
//            Handle updated anchors...
//        }
    }
}
