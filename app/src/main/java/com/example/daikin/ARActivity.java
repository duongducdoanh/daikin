package com.example.daikin;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.ar.core.Anchor;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Pose;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.util.Objects;

public class ARActivity extends AppCompatActivity implements Scene.OnUpdateListener {
    private static final double MIN_OPENGL_VERSION = 3.0;
    private static final String TAG = MainActivity.class.getSimpleName();

    private ArFragment arFragment;
    private AnchorNode currentAnchorNode;
    ModelRenderable cubeRenderable;
    private Anchor currentAnchor = null;

    private MaterialButton btnAdd;
    private Pose currentPose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkIsSupportedDeviceOrFinish(this)) {
            Toast.makeText(getApplicationContext(), "Device not supported", Toast.LENGTH_LONG).show();
        }

        setContentView(R.layout.activity_ar);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        btnAdd = (MaterialButton) findViewById(R.id.add_button);

        btnAdd.setOnClickListener(v -> {
            Frame frame = arFragment.getArSceneView().getArFrame();
            Pose cameraPose = frame.getCamera().getPose();
            if (currentPose == null) {
                currentPose = cameraPose;
                btnAdd.setIconResource(R.drawable.ic_add);
                return;
            }
            btnAdd.setIconResource(R.drawable.ic_done);

            Vector3 point1 = new Vector3(currentPose.tx(), currentPose.ty(), currentPose.tz());
            Vector3 point2 = new Vector3(cameraPose.tx(), cameraPose.ty(), cameraPose.tz());
            float dx = currentPose.tx() - cameraPose.tx();
            float dy = currentPose.ty() - cameraPose.ty();
            float dz = currentPose.tz() - cameraPose.tz();
//
//            ///Compute the straight-line distance.
//            float distanceMeters = (float) Math.sqrt(dx * dx + dy * dy + dz * dz);

            final Vector3 difference = Vector3.subtract(point1, point2);
            final Vector3 directionFromTopToBottom = difference.normalized();
            final Quaternion rotationFromAToB =
                    Quaternion.lookRotation(directionFromTopToBottom, Vector3.up());
            MaterialFactory.makeOpaqueWithColor(getApplicationContext(), new Color(getColor(R.color.teal_200)))
                    .thenAccept(
                            material -> {
/* Then, create a rectangular prism, using ShapeFactory.makeCube() and use the difference vector
       to extend to the necessary length.  */
                                ModelRenderable model = ShapeFactory.makeCube(
                                        new Vector3(.01f, .01f, difference.length()),
                                        Vector3.zero(), material);
/* Last, set the world rotation of the node to the rotation calculated earlier and set the world position to
       the midpoint between the given points . */
                                Node node = new Node();
//                                node.setParent(anchorNode);
                                node.setRenderable(model);
                                node.setWorldPosition(Vector3.add(point1, point2).scaled(.5f));
                                node.setWorldRotation(rotationFromAToB);
                            }
                    );

            currentPose = null;
        });

//        initModel();

        //            if (cubeRenderable == null)
        //                return;
        //
        //            // Creating Anchor.
        //            Anchor anchor = hitResult.createAnchor();
        //            AnchorNode anchorNode = new AnchorNode(anchor);
        //            anchorNode.setParent(arFragment.getArSceneView().getScene());
        //
        //            clearAnchor();
        //
        //            currentAnchor = anchor;
        //            currentAnchorNode = anchorNode;
        //
        //
        //            TransformableNode node = new TransformableNode(arFragment.getTransformationSystem());
        //            node.setRenderable(cubeRenderable);
        //            node.setParent(anchorNode);
        //            arFragment.getArSceneView().getScene().addOnUpdateListener(this);
        //            arFragment.getArSceneView().getScene().addChild(anchorNode);
        //            node.select();
//        arFragment.setOnTapArPlaneListener(this::addLineBetweenHits);
        arFragment.setOnTapArPlaneListener(this::onTapArPlane);

    }

    private void onTapArPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {

    }

    private void addLineBetweenHits(HitResult hitResult, Plane plane, MotionEvent motionEvent) {
        Anchor anchor = hitResult.createAnchor();
        AnchorNode anchorNode = new AnchorNode(anchor);

        if (currentAnchorNode != null) {
            anchorNode.setParent(arFragment.getArSceneView().getScene());
            Vector3 point1, point2;
            point1 = currentAnchorNode.getWorldPosition();
            point2 = anchorNode.getWorldPosition();

            float dx = point1.x - point2.x;
            float dy = point1.y - point2.y;
            float dz = point1.z - point2.z;

            ///Compute the straight-line distance.
            float distanceMeters = (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
//            tvDistance.setText("Distance from camera: " + distanceMeters + " metres");

        /*
            First, find the vector extending between the two points and define a look rotation
            in terms of this Vector.
        */
            final Vector3 difference = Vector3.subtract(point1, point2);
            final Vector3 directionFromTopToBottom = difference.normalized();
            final Quaternion rotationFromAToB =
                    Quaternion.lookRotation(directionFromTopToBottom, Vector3.up());
            MaterialFactory.makeOpaqueWithColor(getApplicationContext(), new Color(getColor(R.color.teal_200)))
                    .thenAccept(
                            material -> {
/* Then, create a rectangular prism, using ShapeFactory.makeCube() and use the difference vector
       to extend to the necessary length.  */
                                ModelRenderable model = ShapeFactory.makeCube(
                                        new Vector3(.01f, .01f, difference.length()),
                                        Vector3.zero(), material);
/* Last, set the world rotation of the node to the rotation calculated earlier and set the world position to
       the midpoint between the given points . */
                                Node node = new Node();
                                node.setParent(anchorNode);
                                node.setRenderable(model);
                                node.setWorldPosition(Vector3.add(point1, point2).scaled(.5f));
                                node.setWorldRotation(rotationFromAToB);
                            }
                    );
//            lastAnchorNode = anchorNode;
        }
        currentAnchorNode = anchorNode;
    }

    public boolean checkIsSupportedDeviceOrFinish(final Activity activity) {

        String openGlVersionString =
                ((ActivityManager) Objects.requireNonNull(activity.getSystemService(Context.ACTIVITY_SERVICE)))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later");
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                    .show();
            activity.finish();
            return false;
        }
        return true;
    }

    private void initModel() {
        MaterialFactory.makeTransparentWithColor(this, new Color(android.graphics.Color.RED))
                .thenAccept(
                        material -> {
                            Vector3 vector3 = new Vector3(0.05f, 0.01f, 0.01f);
                            cubeRenderable = ShapeFactory.makeCube(vector3, Vector3.zero(), material);
                            cubeRenderable.setShadowCaster(false);
                            cubeRenderable.setShadowReceiver(false);
                        });
    }

    private void clearAnchor() {
        currentAnchor = null;


        if (currentAnchorNode != null) {
            arFragment.getArSceneView().getScene().removeChild(currentAnchorNode);
            currentAnchorNode.getAnchor().detach();
            currentAnchorNode.setParent(null);
            currentAnchorNode = null;
        }
    }

    @Override
    public void onUpdate(FrameTime frameTime) {
//        Frame frame = arFragment.getArSceneView().getArFrame();

//        Log.d("API123", "onUpdateframe... current anchor node " + (currentAnchorNode == null));


//        if (currentAnchorNode != null) {
//            Pose objectPose = currentAnchor.getPose();
//            Pose cameraPose = frame.getCamera().getPose();
//
//            float dx = objectPose.tx() - cameraPose.tx();
//            float dy = objectPose.ty() - cameraPose.ty();
//            float dz = objectPose.tz() - cameraPose.tz();
//
//            ///Compute the straight-line distance.
//            float distanceMeters = (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
//            tvDistance.setText("Distance from camera: " + distanceMeters + " metres");
//
//
//            /*float[] distance_vector = currentAnchor.getPose().inverse()
//                    .compose(cameraPose).getTranslation();
//            float totalDistanceSquared = 0;
//            for (int i = 0; i < 3; ++i)
//                totalDistanceSquared += distance_vector[i] * distance_vector[i];*/
//        }
    }
}