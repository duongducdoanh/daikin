package com.example.daikin

object Constants{
    const val maxNumMultiplePoints = 6
    const val multipleDistanceTableHeight = 300

    const val arrowViewSize = 45
}
